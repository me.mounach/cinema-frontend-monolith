FROM nginx:latest
COPY dist/cinema/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf